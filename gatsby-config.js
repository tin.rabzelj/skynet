const plugins = [
  'gatsby-plugin-typescript',
  'gatsby-plugin-react-svg',
  'gatsby-plugin-react-helmet',
  'gatsby-plugin-sharp',
  'gatsby-plugin-catch-links',
  'gatsby-plugin-jss',
  {
    resolve: 'gatsby-plugin-google-fonts',
    options: {
      fonts: ['Roboto:300,400,500,700'],
    },
  },
];

module.exports = {
  siteMetadata: {
    title: 'Platform',
    siteUrl: 'http://localhost:3000',
  },
  plugins,
};
