const path = require('path');

exports.modifyWebpackConfig = ({ config, stage }) => {
  config.merge({
    resolve: {
      alias: {
        '~': path.resolve('src'),
      },
    },
  });
  return config;
};
