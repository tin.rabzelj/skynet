import * as React from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import 'normalize.css';

import { Theme, appTheme, withStyles, WithStyles } from '~/utils';

interface Props {
  children: () => any;
}

class IndexLayout extends React.Component<Props & WithStyles<'@global'>> {
  public render() {
    const { children } = this.props;
    return <MuiThemeProvider theme={appTheme}>{children()}</MuiThemeProvider>;
  }
}

const style = (theme: Theme): any => ({
  '@global': {
    html: {
      height: '100%',
    },
    body: {
      height: '100%',
    },
    '#___gatsby': {
      height: '100%',
    },
  },
});

export default withStyles(style)(IndexLayout);
