import Icon from '@material-ui/core/Icon';
import AddIcon from '@material-ui/icons/Add';
import MenuIcon from '@material-ui/icons/Menu';

export { Icon, AddIcon, MenuIcon };
