import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { Node } from '~/store/graph/types';
import { Theme, withStyles, WithStyles } from '~/utils';
import { Paper } from '~/components/common';
import NodeHeader from '~/components/graph/node-header';

interface State {
  position: {
    x: number;
    y: number;
  };
  relative: {
    x: number;
    y: number;
  };
  dragging: boolean;
}

interface Props {
  node: Node;
}

class NodeComponent extends React.Component<Props & WithStyles, State> {
  public state: State = {
    position: {
      x: 0,
      y: 0,
    },
    relative: {
      x: 0,
      y: 0,
    },
    dragging: false,
  };

  public componentDidUpdate(props: Props, state: State) {
    if (this.state.dragging && !state.dragging) {
      document.addEventListener('mousemove', this.onMouseMove);
      document.addEventListener('mouseup', this.onMouseUp);
    } else if (!this.state.dragging && state.dragging) {
      document.removeEventListener('mousemove', this.onMouseMove);
      document.removeEventListener('mouseup', this.onMouseUp);
    }
  }

  private onMouseDown = (e: React.MouseEvent<HTMLDivElement>) => {
    if (e.button !== 0) {
      return;
    }

    const computedStyle = window.getComputedStyle(ReactDOM.findDOMNode(
      this,
    ) as Element);
    const pos = {
      top: parseInt(computedStyle.top as string),
      left: parseInt(computedStyle.left as string),
    };
    this.setState({
      dragging: true,
      relative: {
        x: e.pageX - pos.left,
        y: e.pageY - pos.top,
      },
    });

    e.stopPropagation();
    e.preventDefault();
  };

  private onMouseUp = (e: MouseEvent) => {
    this.setState({ dragging: false });
    e.stopPropagation();
    e.preventDefault();
  };

  private onMouseMove = (e: MouseEvent) => {
    if (!this.state.dragging) {
      return;
    }

    this.setState({
      position: {
        x: Math.round((e.pageX - this.state.relative.x) / 16) * 16,
        y: Math.round((e.pageY - this.state.relative.y) / 16) * 16,
      },
    });

    e.stopPropagation();
    e.preventDefault();
  };

  public render() {
    const { children, classes, node } = this.props;
    const {
      position: { x, y },
    } = this.state;
    return (
      <Paper className={classes.root} style={{ left: x, top: y }}>
        <NodeHeader onMouseDown={this.onMouseDown} node={node} />
        <div>{children}</div>
      </Paper>
    );
  }
}

const style = (theme: Theme): any => ({
  root: {
    width: 256,
    height: 256,
    position: 'absolute',
  },
});

export default withStyles(style)(NodeComponent);
