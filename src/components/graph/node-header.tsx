import * as React from 'react';

import { Node } from '~/store/types';
import { Theme, withStyles, WithStyles } from '~/utils';
import { Typography } from '~/components/common';

interface Props {
  node: Node;
  onMouseDown?: (e: React.MouseEvent<HTMLDivElement>) => void;
}

class NodeHeaderComponent extends React.Component<Props & WithStyles> {
  public render() {
    const { node, classes, onMouseDown } = this.props;

    return (
      <div className={classes.root} onMouseDown={onMouseDown}>
        <Typography variant="title" color="inherit">
          {node.title}
        </Typography>
      </div>
    );
  }
}

const style = (theme: Theme): any => ({
  root: {
    minHeight: 64,
    padding: [[0, 16]],
    borderRadius: [[2, 2, 0, 0]],
    backgroundColor: theme.palette.grey[100],
    userSelect: 'none',
    display: 'flex',
    position: 'relative',
    alignItems: 'center',
  },
  title: {
    flex: '0 1 auto',
  },
});

export default withStyles(style)(NodeHeaderComponent);
