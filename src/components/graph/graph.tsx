import * as React from 'react';

import { Node } from '~/store/types';
import { Theme, withStyles, WithStyles } from '~/utils';
import { NodeComponent } from '~/components/graph';

interface State {
  position: {
    x: number;
    y: number;
  };
  relative: {
    x: number;
    y: number;
  };
  dragging: boolean;
}

interface Props {
  nodes?: Node[];
}

class GraphComponent extends React.Component<Props & WithStyles, State> {
  public static defaultProps: Props = {
    nodes: [],
  };
  public state: State = {
    position: {
      x: 0,
      y: 0,
    },
    relative: {
      x: 0,
      y: 0,
    },
    dragging: false,
  };

  public componentDidUpdate(props: Props, state: State) {
    if (this.state.dragging && !state.dragging) {
      document.addEventListener('mousemove', this.onMouseMove);
      document.addEventListener('mouseup', this.onMouseUp);
    } else if (!this.state.dragging && state.dragging) {
      document.removeEventListener('mousemove', this.onMouseMove);
      document.removeEventListener('mouseup', this.onMouseUp);
    }
  }

  private onMouseDown = (e: React.MouseEvent<HTMLDivElement>) => {
    if (e.button !== 0) {
      return;
    }

    const pos = this.state.position;
    this.setState({
      dragging: true,
      relative: {
        x: e.pageX - pos.x,
        y: e.pageY - pos.y,
      },
    });

    e.stopPropagation();
    e.preventDefault();
  };

  private onMouseUp = (e: MouseEvent) => {
    this.setState({ dragging: false });
    e.stopPropagation();
    e.preventDefault();
  };

  private onMouseMove = (e: MouseEvent) => {
    if (!this.state.dragging) {
      return;
    }

    this.setState({
      position: {
        x: Math.round((e.pageX - this.state.relative.x) / 16) * 16,
        y: Math.round((e.pageY - this.state.relative.y) / 16) * 16,
      },
    });

    e.stopPropagation();
    e.preventDefault();
  };

  public render() {
    const { classes } = this.props;
    const nodes = this.props.nodes as Node[];
    const {
      position: { x, y },
    } = this.state;
    return (
      <div className={classes.root} onMouseDown={this.onMouseDown}>
        <div className={classes.content} style={{ left: x, top: y }}>
          {nodes.map((node, i) => <NodeComponent key={i} node={node} />)}
        </div>
      </div>
    );
  }
}

const style = (theme: Theme): any => ({
  root: {
    display: 'flex',
    height: '100%',
    backgroundImage: 'url(/images/grid.svg)',
    backgroundRepeat: 'repeat',
    backgroundSize: 32,
    overflow: 'hidden',
  },
  content: {
    width: '100%',
    padding: 16,
    position: 'relative',
    overflow: 'initial',
  },
});

export default withStyles(style)(GraphComponent);
