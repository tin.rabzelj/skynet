import * as React from 'react';

import { Theme, withStyles, WithStyles } from '~/utils';
import {
  AppBar,
  Toolbar,
  Button,
  Typography,
  Tab,
  Tabs,
} from '~/components/common';
import { AddIcon } from '~/components/icons';
import { GraphContainer } from '~/containers/graph';

type Props = {
  addNode: (slug: string) => any;
};

type State = {
  tab: number;
};

class DashboardComponent extends React.Component<Props & WithStyles, State> {
  public state: State = {
    tab: 0,
  };

  handleTabChange = (e: any, value: number) => {
    this.setState({ tab: value });
  };

  handleAddNodeClick = () => {
    this.props.addNode('alert');
  };

  render() {
    const { classes } = this.props;
    const { tab } = this.state;

    return (
      <div className={classes.root}>
        <AppBar position="fixed" color="default">
          <Toolbar>
            <Typography variant="title" color="inherit">
              Title
            </Typography>
          </Toolbar>

          <Tabs
            value={tab}
            onChange={this.handleTabChange}
            indicatorColor="primary"
            textColor="primary"
            scrollable
            scrollButtons="on"
          >
            <Tab label="Item One*" />
            <Tab label="Item Two" />
            <Tab label="Item Three" />
          </Tabs>
        </AppBar>

        <div className={classes.content}>
          <GraphContainer />
          <Button
            className={classes.addNodeButton}
            variant="fab"
            color="primary"
            aria-label="add"
            onClick={this.handleAddNodeClick}
          >
            <AddIcon />
          </Button>
        </div>
      </div>
    );
  }
}

const style = (theme: Theme): any => ({
  root: {
    height: '100%',
    position: 'relative',
  },
  content: {
    height: '100%',
  },
  addNodeButton: {
    position: 'absolute',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
  },
});

export default withStyles(style)(DashboardComponent);
