import * as React from 'react';

import { DashboardContainer } from '~/containers/dashboard';

class IndexPage extends React.Component {
  public render() {
    return <DashboardContainer />;
  }
}

export default IndexPage;
