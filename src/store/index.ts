import { createStore as reduxCreateStore, applyMiddleware } from 'redux';
import { combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';

import {
  reducer as graphReducer,
  State as GraphState,
} from '~/store/graph/reducer';
import graphSaga from '~/store/graph/saga';

const rootReducer = combineReducers({
  graph: graphReducer,
});

const sagaMiddleware = createSagaMiddleware();

const createStore = () => {
  const store = reduxCreateStore(rootReducer, applyMiddleware(sagaMiddleware));
  sagaMiddleware.run(graphSaga);
  return store;
};

export { createStore };

export type RootState = {
  graph: GraphState;
};
