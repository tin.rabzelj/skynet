import { isActionType, Action } from '~/store/helpers';
import { Node } from '~/store/graph/types';
import * as actions from './actions';

export type State = {
  nodes: Node[];
};

const initialState: State = {
  nodes: [],
};

export const reducer = (state = initialState, action: Action<any>): State => {
  if (isActionType(action, actions.clearNodes)) {
    return { ...state, nodes: [] };
  } else if (isActionType(action, actions.addNode.success)) {
    return {
      ...state,
      nodes: [...state.nodes, action.payload],
    };
  }
  return state;
};
