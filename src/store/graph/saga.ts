import { put, takeLatest, call } from 'redux-saga/effects';

import { Action } from '~/store/helpers';
import * as actions from './actions';
import { GraphService } from '~/services';

function* addNode(action: Action<{ slug: string }>) {
  try {
    const node = yield call(GraphService.getNode, action.payload.slug);

    yield put(actions.addNode.success(node));
  } catch (err) {
    yield put(actions.addNode.failure(err));
  }
}

function* saga() {
  yield takeLatest(actions.addNode.request.type, addNode);
}

export default saga;
