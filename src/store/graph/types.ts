/*
export type Node = {
  slug: string;
  title: string;
};
*/

export class Node {
  constructor(public slug: string, public title: string) {}
}
