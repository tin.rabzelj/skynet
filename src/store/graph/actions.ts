import { createAction, createAsyncAction } from '~/store/helpers';
import { Node } from '~/store/graph/types';

export const clearNodes = createAction('GRAPH_CLEAR_NODES');
export const addNode = createAsyncAction<{ slug: string }, Node>(
  'GRAPH_ADD_NODE',
);
