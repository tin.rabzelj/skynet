import { Dispatch } from 'redux';
import { RootState } from '~/store';
import * as actions from './actions';

export default {
  state: {
    getNodes: (state: RootState) => state.graph.nodes,
  },
  action: {
    addNode: (dispatch: Dispatch) => (slug: string) =>
      dispatch(actions.addNode.request({ slug })),
  },
};
