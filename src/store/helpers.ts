export type Action<T> = {
  type: string;
  payload: T;
};

interface ActionCreator<P> {
  type: string;
  (payload: P): Action<P>;
}

interface AsyncActionCreator<R, S> {
  request: ActionCreator<R>;
  success: ActionCreator<S>;
  failure: ActionCreator<Error>;
}

export function createAction<P>(type: string): ActionCreator<P> {
  return Object.assign((payload: P) => ({ type, payload }), { type });
}

export function createAsyncAction<R, S>(
  type: string,
): AsyncActionCreator<R, S> {
  const request = `${type}_REQUEST`;
  const success = `${type}_SUCCESS`;
  const failure = `${type}_FAILURE`;
  return {
    request: Object.assign((payload: R) => ({ type: request, payload }), {
      type: request,
    }),
    success: Object.assign((payload: S) => ({ type: success, payload }), {
      type: success,
    }),
    failure: Object.assign((payload: Error) => ({ type: failure, payload }), {
      type: failure,
    }),
  };
}

export function isActionType<P>(
  action: Action<any>,
  actionCreator: ActionCreator<P>,
): action is Action<P> {
  return action.type === actionCreator.type;
}
