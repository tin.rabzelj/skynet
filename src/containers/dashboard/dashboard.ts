import { connect } from 'react-redux';

import { DashboardComponent } from '~/components/dashboard';
import { Dispatch } from '~/store/types';
import graphMap from '~/store/graph/map';

const mapDispatchToProps = (dispatch: Dispatch) => ({
  addNode: graphMap.action.addNode(dispatch),
});

const DashboardContainer = connect(null, mapDispatchToProps)(
  DashboardComponent,
);

export default DashboardContainer;
