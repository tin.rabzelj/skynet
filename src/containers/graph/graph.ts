import { connect } from 'react-redux';

import { RootState } from '~/store';
import graphMap from '~/store/graph/map';
import { GraphComponent } from '~/components/graph';

const mapStateToProps = (state: RootState): any => ({
  nodes: graphMap.state.getNodes(state),
});

const GraphContainer = connect(mapStateToProps)(GraphComponent);

export default GraphContainer;
