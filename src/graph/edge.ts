import { Hook } from './hook';

export class Edge {
  constructor(public a: Hook, public b: Hook) {}
}
