import { Hook } from './hook';

export abstract class Node {
  private _inputs: Hook[];
  private _outputs: Hook[];

  constructor(public slug: string, public title: string) {
    this._inputs = [];
    this._outputs = [];
  }

  public addInput(hook: Hook) {
    hook.node = this;
    this._inputs.push(hook);
  }

  public addOutput(hook: Hook) {
    hook.node = this;
    this._inputs.push(hook);
  }

  public get inputs(): Hook[] {
    return this._inputs;
  }

  public get outputs(): Hook[] {
    return this._outputs;
  }
}
