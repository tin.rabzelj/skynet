import { Node } from './node';
import { ValueType } from './types';

export class Hook {
  public node: Node;

  constructor(public type: ValueType) {}
}
