import { Edge } from './edge';
import { Node } from './node';

export class Graph {
  private _nodes: Node[];

  constructor() {}

  public get nodes(): Node[] {
    return this._nodes;
  }
}
