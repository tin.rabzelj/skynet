import { Theme, createMuiTheme } from '@material-ui/core/styles';

export { Theme };
export const appTheme: Theme = createMuiTheme();
