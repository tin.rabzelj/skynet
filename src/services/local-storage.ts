import { RootState } from '~/store/types';

const STORAGE_KEY = `__STATE_${process.env.APP_VERSION}__`;

class LocalStorageService {
  public save(state: RootState): boolean {
    if (!localStorage) {
      return false;
    }
    try {
      localStorage.setItem(STORAGE_KEY, JSON.stringify(state));
      return true;
    } catch (e) {
      throw new Error('LocalStorageService: save failed');
    }
  }

  public load(): RootState | undefined {
    if (!localStorage) {
      return;
    }

    try {
      const state = localStorage.getItem(STORAGE_KEY);
      if (state) {
        return JSON.parse(state) as RootState;
      } else {
        return;
      }
    } catch (e) {
      throw new Error('LocalStorageService: load failed');
    }
  }

  public clear(): void {
    if (localStorage) {
      localStorage.removeItem(STORAGE_KEY);
    }
  }
}

export default LocalStorageService;
