import { Node } from '~/store/graph/types';

interface GraphService {
  getNode(slug: string): Promise<Node>;
}

class GraphService {
  getNode(slug: string) {
    const node = new Node(slug, 'New node');
    return Promise.resolve(node);
  }

  private static impl: GraphService = new GraphService();

  public static getNode(slug: string): Promise<Node> {
    return GraphService.impl.getNode(slug);
  }
}

export default GraphService;
